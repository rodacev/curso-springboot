package com.bolsadeideas.springboot.web.app.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/variables")

public class EjemplosVariablesRutaController {
	
	@Value("${texto.variablescontroller.index.titulo}")
	private String textoIndex;
	
	@Value("${texto.variablescontroller.variables.titulo}")
	private String textoVariablesDos;
	
	@GetMapping("/")
	public String index(Model model) {
		model.addAttribute("titulo",textoIndex);
		return "variables/index";
	}
	
	@GetMapping("/string/{texto}")
	public String variables(@PathVariable String texto, Model model) {
		model.addAttribute("titulo", textoVariablesDos);
		model.addAttribute("resultado", "El texto enviado en la ruta es : " + texto);
		return "variables/ver";
	}
	
	
//	@GetMapping("/string/{texto}/{numero}")
//	public String variables(@PathVariable String texto, @PathVariable Integer numero, Model model) {
//		model.addAttribute("titulo", "Recibir parámetros de la ruta (@PathVariable)");
//		model.addAttribute("resultado", "El texto enviado en la ruta es : " + texto
//				+ " y el numero enviado en el path es : " + numero);
//		return "variables/ver";
//	}
	
	@GetMapping("/string/{numero_uno}/{numero_dos}")
	public String variables(@PathVariable int numero_uno, @PathVariable int numero_dos, Model model) {
		int suma = numero_uno + numero_dos;
		model.addAttribute("titulo", "Recibir parámetros de la ruta (@PathVariable)");
		model.addAttribute("resultado", "El resultado de la suma del numero " + numero_uno
				+ " y el numero " + numero_dos + " es igual a : " + suma);
		return "variables/ver";
	}
	
	
	

}
