package com.bolsadeideas.springboot.web.app.controllers;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;

import com.bolsadeideas.springboot.web.app.models.Usuario;


@Controller
@RequestMapping("/app") // PARA DAR UNA RUTA BASE
public class IndexController {
	
	// CON ESTAS VARIAS VAMOS A DESACOLPLAR LOS TEXTOS DEL CONTROLADOR INYECTANDOLOS DESDE EL ARCHIVO applications.properties UTILIZANDO
	// LA ANOTACIÓN @VALUES
	
	@Value("${texto.indexcontroller.index.titulo}")
	private String textoIndex;
	
	@Value("${texto.indexcontroller.perfil.titulo}")
	private String textoPerfil;
	
	@Value("${texto.indexcontroller.listar.titulo}")
	private String textoListar;
	
//	@RequestMapping(value = "/index", method = RequestMethod.GET) // SE PUEDE OMITIR EL "method = RequestMethod.GET"
//	public String index() {
//		
//		return "index";
//	}
	
	
	// Usando Interface Model
	@GetMapping({"/index","","/","home"}) // 
	public String index(Model model) {
		model.addAttribute("titulo", textoIndex);
		return "index";
	}
	
	@GetMapping("/perfil") // 
	public String perfil(Model model) {
		
		Usuario usuario = new Usuario();
		usuario.setNombre("Rodrigo");
		usuario.setApellido("Acevedo");
		usuario.setEmail("rodacev@gmail.com");
		
		
		model.addAttribute("usuario", usuario);
		model.addAttribute("titulo", textoPerfil.concat(usuario.getNombre()));
		return "perfil";
	}
	
	
	
	@GetMapping("/listar")
	public String listar(Model model) {
		
		//List<Usuario> listaUsuarios = new ArrayList<>();
		
		//o usando la clase Arrays con metodo asList... convierte elementos separados por comas en una lista.
//		List<Usuario> listaUsuarios = Arrays.asList(new Usuario("Yanina", "Berrios", "yanina@yanina.cl"), 
//				new Usuario("Nancy", "Palacios", "nancy@nancy.cl"), new Usuario("Micho", "Acevedo", "micho@micho.cl"),
//				new Usuario("Ambrosio", "Berrios", "ambrosio@ambrosio.cl"));
		
		// COMENTO TODO LO DE ARRIBA POR QUE LO REEMPLAZO CON LA ANTOCION ModelAttribute DE ABAJO
		// SE USA UNO U OTRO DEPENDIENDO DE SI VPY A UTILIZAR UN OBJETO EN VARISO METODOS O VISTAS.
		

		
		model.addAttribute("titulo", textoListar);
		
		return "listar";
	}
	
	@ModelAttribute("listaUsuarios")
	public List<Usuario> poblarUsuarios(){
		List<Usuario> listaUsuarios = Arrays.asList(new Usuario("Yanina", "Berrios", "yanina@yanina.cl"), 
				new Usuario("Nancy", "Palacios", "nancy@nancy.cl"), new Usuario("Micho", "Acevedo", "micho@micho.cl"),
				new Usuario("Ambrosio", "Berrios", "ambrosio@ambrosio.cl"));
		return listaUsuarios;
	}
	
	

	
	// usando ModelMap
//	@GetMapping({"/index","/","home"}) // 
//	public String index(ModelMap model) {
//		model.addAttribute("titulo", "Hola Spring Framework con ModelMap");
//		return "index";
//	}
	
	// Unsando con Map
//	@GetMapping({"/index","/","home"}) // 
//	public String index(Map<String, Object> map) {
//		map.put("titulo", "Hola Spring Framework con Map");
//		return "index";
//	}
	
	// Usando ModelAndView
//	@GetMapping({"/index","/","home"}) // 
//	public ModelAndView index(ModelAndView mv) {
//		mv.addObject("titulo", "Hola Spring Framework con ModelAndView");
//		mv.setViewName("index");
//		return mv;
//	}
	
	
	
	
	
	

}
